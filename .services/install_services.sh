#!/bin/bash
# Don't forget to make this file executable first:
# chmod a+rx

# copy the timed maintenance.sh files to /home/ and make executable
sudo cp maintenance.sh /home/maintenance.sh && sudo chmod a+rx /home/maintenance.sh

# make upstream maintenance instructions executable
chmod a+rx /home/tna/database/maintenance_tasks.sh

# copy the service unit files to systemd
sudo cp *.service /lib/systemd/system/
# copy the timer unit files to systemd
sudo cp *.timer /lib/systemd/system/

# tell systemd to recognize our new services
sudo systemctl daemon-reload

# tell systemd that we want our service to start on boot
for file in *.service
do
    echo enabling ${file}
    sudo systemctl enable ${file}

done

# tell systemd that we want our timer to start on boot
for file in *.timer
do
    echo enabling ${file}
    sudo systemctl enable ${file}

done
