#!/bin/bash
#===============================================================================
# Pull firmware modifications
cd /home/tna/pydevctrl-firmware
git pull

#===============================================================================
# Run client-specific instructions
sh /home/tna/database/maintenance_tasks.sh

#===============================================================================
# Push data
cd /home/tna/database
git pull
git add --all
git commit --author='clientRPi <>' -m "Update remote"
git push
#===============================================================================
