#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 16:40:15 2018

@author: nils
"""
# Dependencies:
# 3rd party; shipped with conda
import numpy as np
import matplotlib.pyplot as plt
import time
from datetime import datetime
import os
import getpass
import traceback
import json

# Own stuff
from modules import GPIO

# Suppress Warnings
import warnings
warnings.filterwarnings("ignore")


class Measurement():

    def __init__(self, OO, **settings):

        # set directories and file names for logging
        user = getpass.getuser()
        self.log_dir = '/home/%s/database/'%(user)
        self.log_file = 'README.md'
        try:
            self.name = settings['experiment']

            self.spectrometer = OO[settings['detector']]
            self.sensors = settings['sensors']
            for sensor in self.sensors.keys():
                self.sensors[sensor]['switch'] = GPIO.Switch(
                                                    self.sensors[sensor]['GPIO-pin'])
            self.directory = settings['dir']
            if not os.path.exists(self.directory):
                os.mkdir(self.directory)

            if not 't0' in settings.keys():
                self.t0 = time.time()
                settings['t0'] = self.t0
            else:
                self.t0 = settings['t0']

            def jdefault(o):
                return o.__dict__

            with open(os.path.join(self.directory, 'acquisition.settings'), 'w') as settings_bak:
                json.dump(settings, settings_bak, indent=4, default=jdefault)

            self.log('# '+ self.name, time=False)

        except Exception:
                self.log('An error occurred while creating the Measurement instance:')
                with open(os.path.join(self.log_dir, self.log_file), 'a+') as f:
                    traceback.print_exc(file=f)
                    f.write('\n\n')

    def log(self, incidence, time=True):
        """
        add message-lines to log.txt
        """
        with open(os.path.join(self.log_dir,self.log_file), 'a+') as log:
            if time:
                log_message = '**'+str(datetime.now()).split('.')[0] + '**\t' + incidence +'\n\n'
            else:
                log_message =  incidence +'\n\n'
            log.write(log_message)

    def acquisition_sequence(self, detector, IT, N, gpio_switch):
        """
        base function to be used in more complex routines
        """
        detector.set_IT(IT)
        Idark_n, I_n= [],[]

        for i in range(N):

            gpio_switch.low()
            Idark_n.append(detector.multiple_spectra(1))
            time.sleep(.5)
            gpio_switch.high()
            I_n.append(detector.multiple_spectra(1))
            time.sleep(.5)
        gpio_switch.low()

        for i in range(N):
            I_n[i]['intensities'] = I_n[i]['intensities'] - Idark_n[i]['intensities']

        I = {'intensities':[I_n[i]['intensities'] for i in range(N)],
            'IT':detector.IT,
            'N':N,
            'detector':detector.name}
        return I

    def save(self, *args, path='.', as_txt=False):
        data = {}
        for arg in args:
            data.update(arg)
        # store data to file
        if path=='.':
            path=os.path.join(self.directory, data['timestamp']+'_'+self.name)
        if as_txt:
            storeTo = path+'.csv'
            # create Header
            with open(str(storeTo), 'w') as fopen:
                for key in data.keys():
                    if not key=='intensities':
                        fopen.write(key + ': ' + str(data[key])+'\n')
                fopen.write('#===============#\n')
                headers='Wavelength [nm]'
                for i in range(data['N']):
                    headers = headers +',I%s'%(i+1)
                headers=headers+',mean,std'
                fopen.write('%s\n'%headers)
            I=data['intensities']
            wl=data['wavelength']
            columns = [wl]+I+[np.mean(I,axis=0),np.std(I, axis=0)]
            self.arrays2txt(columns, str(storeTo))

        else:
            storeTo = path
            np.savez_compressed(storeTo, **data)

    def arrays2txt(self, arraylist, filename):
        """
        save multiple columns of array data to a single (tab separated) text file
        """
        storeTo=str(filename)
        lines=[]

        for i, entry in enumerate(arraylist[0]):
            line= str(entry)
            # print(len(arraylist))
            if len(arraylist)==2:
                line = line + ',' + str(arraylist[1][i])
            else:
                for array in arraylist[1:]:
                    line = line + ',' + str(array[i])
            line= line+'\n'
            lines.append(line)

        with open(str(storeTo), 'a') as fopen:
            for line in lines:
                fopen.write(line)

    def get_intensities(self, N, save=False, label=None, as_txt = False, confirm = False):
        timestamp = datetime.now().strftime('%Y_%m_%d-%Hh%M')
        for i,key in enumerate(self.sensors.keys()):
            I = self.acquisition_sequence(self.spectrometer, self.sensors[key]['IT'], N, self.sensors[key]['switch'])
            fig = plt.figure()
            fig.suptitle(key)
            axis = fig.add_subplot(111)
            metadata = {'experiment':self.name,
                        'sensor':key,
                        'timestamp':timestamp,
                        'wavelength':self.spectrometer.WL}
            if label:
                metadata['experiment'] =  self.name + ' | ' + label
            else:
                label = self.name

            for dataset in I['intensities']:
                axis.plot(self.spectrometer.WL, dataset, label=label)
            fig.show()

            if save:
                storeTo = os.path.join(self.directory, timestamp+'_'+self.name+'_'+key)
                self.save(metadata, I,  path=storeTo, as_txt=as_txt)
                fig.savefig(storeTo+'.png', format='png')
            del I

            if confirm:
                input('Press ENTER to proceed.')

    def start_repetition(self):

        self.log('Starting data acquisition. Saving data to: ' + self.directory)
        indicator = GPIO.Switch(25)
        try:
            while True:
                try:
                    with open(os.path.join(self.directory,'acquisition.params'), 'r') as params_bak:

                        params = json.load(params_bak)

                        stop = params['stop']
                        period_minutes = params['period [min]']
                        N = params['N']

                    if stop:
                        indicator.low()
                        self.log('Acquisition cycle stopped by user.')
                        break

                    elif not stop:
                        #start to assess total duration of measurement
                        acquisition_init = time.time()

                        timestamp = datetime.now().strftime('%Y_%m_%d-%Hh%M')
                        for key in self.sensors.keys():
                            I = self.acquisition_sequence(self.spectrometer,
                            self.sensors[key]['IT'],
                            N,
                            self.sensors[key]['switch'])
                            metadata = {'experiment':self.name,
                                        'sensor':key,
                                        'timestamp':timestamp,
                                        'period [min]':period_minutes,
                                        'time':[self.t0, acquisition_init-self.t0]}
                            storeTo = os.path.join(self.directory, timestamp+'_'+self.name+'_'+key)
                            self.save(metadata, I, path=storeTo)
                            del I
                        # end assessment of duration of measurement
                        acquisition_duration = time.time() - acquisition_init

                        idle_time = period_minutes*60 - acquisition_duration
                        idle_init = time.time()
                        while True:
                            with open(os.path.join(self.directory,'acquisition.params'), 'r') as params_bak:

                                params = json.load(params_bak)

                                stop = params['stop']
                                period_minutes = params['period [min]']
                                N = params['N']
                            if stop:
                                break
                            else:
                                seconds_passed = time.time() - idle_init
                            #    print(seconds_passed, 'seconds passed')
                                if seconds_passed >= (idle_time):
                            #        print('defined idle time was',idle_time )
                                    indicator.high()
                                    break
                                else:
                                    indicator.high()
                                    time.sleep(.2)
                                    indicator.low()
                                    time.sleep(1.5)
                    else:
                        time.sleep(.25)

                except Exception:
                    indicator.low()
                    with open(os.path.join(self.log_dir,self.log_file), 'a+') as f:
                        traceback.print_exc(file=f)
                        f.write('\n\n')
        except KeyboardInterrupt:
            indicator.low()
            message ='Acquisition cycle stopped by user.'
            print(message)
            self.log(message)
