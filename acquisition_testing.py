#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 18:15:53 2018

@author: nils
"""
# =============================================================================
# Dependencies
# =============================================================================

import os as os
import getpass

import numpy as np
import json
import traceback

from datetime import datetime

#import matplotlib.pyplot as plt
#from matplotlib.animation import FuncAnimation
#import ipywidgets as widgets
#from ipywidgets import interact
#from IPython.display import display

import seabreeze as sb
sb.use('cseabreeze')
import seabreeze.spectrometers as sbs

# Own stuff
import modules
from modules import GPIO
from modules.MyOpticsLab import MyOpticsLab
from modules.MyMeasurement import Measurement

def jdefault(o):
    return o.__dict__

# =============================================================================
# Get MyLab up and running
# ========================================================='log.txt'====================
# invoke and initialize MyLabOptica instance

MyLab = MyOpticsLab(os, sl=None, sbs=sbs)

#import matplotlib.animation as animation

if len(MyLab.devices) > 0:
    # Setup Spectrometers
    from modules.MySpectrometer import MySpectrometer
    MyLab.NonlinCorrect = False
    MyLab.DarkCurrentCorrect = False
    OO = {}

    # Create ViewPort for live plot of intensities
#    ViewPort = plt.figure('Live Data Feed', figsize=(8,6))

#    spec_count = len(MyLab.devices)
#    if spec_count == 1:
#        slots = [ViewPort.subplots(spec_count, sharex=True, sharey=True)]
#    else:
#        slots = ViewPort.subplots(spec_count, sharex=True, sharey=True)

    for i,device in enumerate(MyLab.devices):
        # Create a Seabreeze instance
        OO[MyLab.SeaBreeze_dict[str(device)]] = MySpectrometer(sb, sbs, MyLab, device)
        OO[MyLab.SeaBreeze_dict[str(device)]].name = MyLab.SeaBreeze_dict[str(device)]
        OO[MyLab.SeaBreeze_dict[str(device)]].set_IT(MyLab.IT)
        print('Connected to **' + MyLab.SeaBreeze_dict[str(device)] + '**')
        # Start corresponding data Live Feed
#        OO[MyLab.SeaBreeze_dict[str(device)]].start_stream(ViewPort, slots[i])
#    MyLab.LabControls_show()

else:
    print('No Ocean Optics devices connected.')


# =============================================================================
# Initiate Measurement
# =============================================================================

detector = OO['USB2000plusXR']
sensors = {'biofilm':{'GPIO-pin':23, 'IT':3000},
           'referencia':{'GPIO-pin':24, 'IT':3000}
=======
sensors = {'biofilm':{'GPIO-pin':23, 'IT':30000},
           'referencia':{'GPIO-pin':24, 'IT':30000}
          }
experiment = 'Calibration-R2v052@EMATSA'
settings = {'experiment': experiment,
          'detector':detector.name,
          'sensors':sensors,
          'dir': os.path.join('/home/tna/database/data/', experiment)}

M = Measurement(OO, **settings)
params = {'period [min]':3,
          'N':1,
          'stop':True}

with open(os.path.join(settings['dir'], 'acquisition.params'), 'w') as params_bak:
    json.dump(params, params_bak, indent=4, default=jdefault)


M.test_intensities(3, save=True) 
#M.start_repetition() # para iniciar la acquisición periodica

print('done.')
